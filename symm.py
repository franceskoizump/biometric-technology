import numpy as np
import cv2
import matplotlib.pyplot as plt

face_cascade = cv2.CascadeClassifier('/usr/share/opencv4/haarcascades/haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('/usr/share/opencv4/haarcascades/haarcascade_eye.xml')

def dist(L, R):
    aa = np.absolute(L-cv2.flip(R, 1))
    return np.sum(aa)

def symmX(cimg, fromH, toH, wl):
    xt = 0 + wl + 1
    xt_1 = wl;
    while (dist(cimg[fromH:toH, xt-wl:xt], cimg[fromH:toH, xt:xt+wl]) <  dist(cimg[fromH:toH, xt_1-wl:xt_1], cimg[fromH:toH, xt_1:xt_1+wl])):
        xt = xt + 1
        xt_1 = xt_1 + 1
    return xt;


#for i in ["я"]:#range(1, 14):
def do_job(name):
    #img = cv2.imread(str(i) + '.jpg')
    img = cv2.imread(name)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    for (x,y,w,h) in faces:
        print(name);
        cimg = img[y:y+h, x:x+w]
        wl = 295 
#        xt = 0 + wl + 1
#        xt_1 = wl;
#        while (dist(cimg[0:h, xt-wl:xt], cimg[0:h, xt:xt+wl]) >  dist(cimg[0:h, xt_1-wl:xt_1], cimg[0:h, xt_1:xt_1+wl])):
#            xt = xt + 1
#            xt_1 = xt_1 + 1
        xt = symmX(cimg,0, h, wl)
        LEFT = cimg[0:h, 0:xt]
        RIGHT = cimg[0:h, xt:w]
        x1 = symmX(cimg, 200, 300, 200);
        x2 = symmX(RIGHT, 0, h, 100);
        cv2.line(cimg, (x1, 0), (x1, h), (255, 0, 0))
        cv2.line(cimg, (xt, 0), (xt, h), (0, 255, 0))
        cv2.line(cimg, (xt+x2, 0), (xt+x2, h), (255, 0, 0))
        plt.imshow(cimg)
        plt.show()


do_job("я.jpg")
