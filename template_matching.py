import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

templates = ['fullface.jpg', 'bottom.jpg', 'eye.jpg']
for t in templates:
    template = cv.imread(t, 0)
    w, h = template.shape[::-1]
    for i in range(1, 14):
        img = cv.imread(str(i) + '.jpg', 0)
        method = cv.TM_CCOEFF
        res = cv.matchTemplate(img,template,method)
        min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
        
        top_left = max_loc
        bottom_right = (top_left[0] + w, top_left[1] + h)
        img = cv.cvtColor(img, cv.COLOR_GRAY2RGB)
        cv.rectangle(img,top_left, bottom_right, (255, 0, 0), 3)
        plt.imshow(img,cmap = 'gray')
        plt.savefig(str(i) + '_' + t)
        plt.clf()
